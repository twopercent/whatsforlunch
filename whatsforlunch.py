#!/usr/bin/python3

import configparser
import requests
import datetime

config = configparser.ConfigParser()
config.read('school.config')
SchoolId = config.get('adams', 'SchoolId')
ServingLine = config.get('adams', 'ServingLine')
MealType = config.get('adams', 'MealType')
MenuSection = config.get('adams', 'MenuSection')

ServingDate = datetime.date.today()

payload = {'SchoolID': SchoolId, 'ServingLine': ServingLine, 'ServingDate': ServingDate, 'MealType': MealType}
r = requests.get('https://webapis.schoolcafe.com/api/CalendarView/GetDailyMenuitems', params=payload)

#print(r.text)

data = r.json()

#Get MenuSection from here
#print(data.keys())

try:
    #Loop through items in the menu and print the description of each
    for item in data[MenuSection]:
        print(item["MenuItemDescription"])
except:
    print("I don't have a menu for today")
